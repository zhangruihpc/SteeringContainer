**Note:** 
This documentation is to test your `search_space.json`.
Please refer to [evaluation container](https://gitlab.cern.ch/zhangruihpc/EvaluationContainer) for **HPO task submission**.

## Run from remote image built by gitlab CI
Make sure your computer has [Docker Desktop](https://docs.docker.com/get-docker/) installed.

```
docker run --rm -it -v $(pwd):/HPO gitlab-registry.cern.ch/zhangruihpc/steeringcontainer:latest /bin/bash
```

We will be using [`hpogrid`](https://gitlab.cern.ch/aml/hyperparameter-optimization/alkaid-qt/hpogrid) to generate hyperparameter points using different libraries.
The package is installed in the container.


Verify `search_space.json` by:
```
hpogrid generate --n_point 3 --max_point 10 --space search_space.json --outfile output.json -l skopt

```