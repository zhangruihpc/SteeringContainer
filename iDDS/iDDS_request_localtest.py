#!/usr/bin/env python
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# http://www.apache.org/licenses/LICENSE-2.0OA
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2020


"""
Test hyper parameter optimization docker container in local env.

- Install iDDS
git clone https://github.com/HSF/iDDS.git (or git clone https://github.com/wguanicedew/iDDS.git)
git checkout --track remotes/origin/dev
pip install idds-common idds-client requests
cd ~/work/container/iDDS/common
pip install .
cd ~/work/container/iDDS/client
pip install .

- Test request
python iDDS_request_localtest.py
"""

import json
import os
import random
import re
import subprocess
import tempfile
import argparse
import shutil, glob
# from uuid import uuid4 as uuid

# from idds.client.client import Client
# from idds.common.utils import replace_parameters_with_values, run_command
from idds.common.constants import RequestType, RequestStatus
# from idds.common.utils import get_rest_host


def get_test_codes():
    dir_name = os.path.dirname(os.path.abspath(__file__))
    test_codes = os.path.join(dir_name, 'activelearning_test_codes/activelearning_test_codes.tgz')
    return test_codes


def run_command(cmd):
    """
    Runs a command in an out-of-procees shell.
    """
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
    stdout, stderr = process.communicate()
    if stdout is not None and type(stdout) in [bytes]:
        stdout = stdout.decode()
    if stderr is not None and type(stderr) in [bytes]:
        stderr = stderr.decode()
    status = process.returncode
    return status, stdout, stderr


def replace_parameters_with_values(text, values):
    """
    Replace all strings starting with '%'. For example, for this string below, it should replace ['%NUM_POINTS', '%IN', '%OUT']
    'run --rm -it -v "$(pwd)":/payload gitlab-registry.cern.ch/zhangruihpc/endpointcontainer:latest /bin/bash -c "echo "--num_points %NUM_POINTS"; /bin/cat /payload/%IN>/payload/%OUT"'
    :param text
    :param values: parameter values, for example {'NUM_POINTS': 5, 'IN': 'input.json', 'OUT': 'output.json'}
    """
    for key in values:
        key1 = '%' + key
        text = re.sub(key1, str(values[key]), text)
    return text


def create_temp_dir():
    dirpath = tempfile.mkdtemp()
    return dirpath


def create_idds_input_json(job_dir, req_meta, space_file):
    # input_json = 'idds_input_%s.json' % random.randint(1, 1000)
    input_json = 'input.json'
    if 'initial_points' in req_meta:
        points = req_meta['initial_points']
    else:
        points = []

    opt_space = json.load(open(space_file))
    opt_points = {'points': points, 'opt_space': opt_space}
    if 'opt_space' in req_meta:
        opt_space = req_meta['opt_space']
        opt_points['opt_space'] = opt_space
    with open(os.path.join(job_dir, input_json), 'w') as f:
        json.dump(opt_points, f)
    return input_json


def get_output_json(req_meta):
    output_json = None
    if 'output_json' in req_meta:
        output_json = req_meta['output_json']
    return output_json


def get_exec(job_dir, req_meta, space_file):
    if 'max_points' in req_meta:
        max_points = req_meta['max_points']
    else:
        max_points = 20
    if 'num_points_per_generation' in req_meta:
        num_points = req_meta['num_points_per_generation']
    else:
        num_points = 10
    num_init_points = 0
    if 'initial_points' in req_meta:
        num_init_points = len(req_meta['initial_points'])
    num_points = min(max_points, num_points) - num_init_points

    idds_input_json = create_idds_input_json(job_dir, req_meta, space_file)
    output_json = get_output_json(req_meta)

    param_values = {'MAX_POINTS': max_points,
                    'NUM_POINTS': num_points,
                    'IN': idds_input_json,
                    'OUT': output_json}

    executable = req_meta['executable']
    arguments = req_meta['arguments']
    executable = replace_parameters_with_values(executable, param_values)
    arguments = replace_parameters_with_values(arguments, param_values)

    exect = executable + ' ' + arguments
    return exect


def get_req_properties():
    req_properties = {
        # 'scope': 'data15_13TeV',
        # 'name': 'data15_13TeV.00270949.pseudo.%s' % str(uuid()),
        'requester': 'panda',
        'request_type': RequestType.HyperParameterOpt,
        'transform_tag': 'prodsys2',
        'status': RequestStatus.New,
        'priority': 0,
        'lifetime': 30,
        'request_metadata': {'workload_id': '30000000', 'sandbox': None, 'executable': 'docker', 'arguments': 'run -v "$(pwd)":/HPOiDDS gitlab-registry.cern.ch/zhangruihpc/steeringcontainer:0.0.1 /bin/bash -c "hpogrid generate --n_point=%NUM_POINTS --max_point=%MAX_POINTS --infile=/HPOiDDS/%IN  --outfile=/HPOiDDS/%OUT -l=skopt"', 'output_json': 'output.json', 'max_points': 10, 'num_points_per_generation': 1}
    }
    return req_properties


def run_test(current, space_file):
    job_dir = create_temp_dir()
    print('job dir: %s' % job_dir)
    os.chdir(job_dir)
    print("$pwd: %s" % os.getcwd())
    req = get_req_properties()
    req_meta = req['request_metadata']
    exe = get_exec(job_dir, req_meta, os.path.join(current, args.searchSpaceFile))
    print('executable: %s' % exe)
    status, output, error = run_command(exe)
    print('status: %s, output: %s, error: %s' % (status, output, error))
    output_json = get_output_json(req_meta)
    if not output_json:
        print('output_json is not defined')
    else:
        print('output_json is', output_json)
        output_json = os.path.join(job_dir, output_json)
        if not os.path.exists(output_json):
            print("Failed: output_json is not created")
        else:
            try:
                with open(output_json, 'r') as f:
                    data = f.read()
                outputs = json.loads(data)
                print("outputs: %s" % str(outputs))
                if type(outputs) not in [list, tuple]:
                    print("Failed: outputs is not a list")
                for f in glob.glob(os.getcwd()+'/*json'):
                    print('copy', f, 'to', current)
                    shutil.copy(f, current)

            except Exception as ex:
                print("Failed to parse output_json: %s" % ex)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--searchSpaceFile', action='store', help='input configuration', required=True)
    args = parser.parse_args()
    run_test(os.getcwd(), args.searchSpaceFile)

