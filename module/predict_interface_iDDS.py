#!/usr/bin/env python
# Rui Zhang 5.2020
# rui.zhang@cern.ch

from argparse import ArgumentParser
from os import environ, makedirs, path
import numpy as np
import json
from predict_util import SavePredictedPoints, DefineOptimizer, numpytypeconverter

def main(args):
    
    new_point_json = args.output
    assert (new_point_json.endswith('.json')), f'output must be JSON, now is {new_point_json}'
        
    if args.fix_seed:
        np.random.seed(args.fix_seed)


    # search space in idds_input.json is recommended to used therefore is dummy
    input_points, dummy_search_space = read_idds_points(args.input_points)
    sampled_points = generate_new_points(args, input_points)
    SavePredictedPoints(new_point_json, sampled_points)

def generate_new_points(args, input_points):

    library = args.library
    name = args.experiment
    max_points = args.max_points
    num_points = args.num_points

    # search space defined by user
    from search_space_convertor import retrieve_search_space
    space_key = None if args.input_space else args.search_space
    search_space, search_name = retrieve_search_space(library, args.input_space, name, space_key)

    if len(input_points) > max_points:
        return []
    num_points = min(num_points, max_points - len(input_points))
    
    import nevergrad as ng

    optimizer = DefineOptimizer(args, search_space)

    unfinished_points = []
    for input_point in input_points:
        point, loss = input_point
        optimizer.suggest(**point)
        candicate = optimizer.ask()
        if loss:
            optimizer.tell(candicate, loss)
        else:
            unfinished_points.append(candicate)


    new_points = []
    for _ in range(num_points):
        x = optimizer.ask()
        if x in unfinished_points:
            continue
        if library == 'skopt':
            x = [numpytypeconverter(i) for i in x]
            point = dict(zip(search_name, x))
        elif library == 'nevergrad':
            point = x.value[1]
        new_points.append(point)
    return new_points

def read_idds_points(input):
    points = None
    opt_space = None
    with open(input) as input_json:
        opt_points = json.load(input_json)
    if 'points' in opt_points:
        points = opt_points['points']
    if 'opt_space' in opt_points:
        opt_space = opt_points['opt_space']
    return points, opt_space


if __name__ == '__main__':

    """Get arguments from command line."""
    parser = ArgumentParser(description="Workforce to optimise domain space")
    parser.add_argument('--input-space', default = '../share/search_space.json', type = str, help = 'Name of the json file')
    parser.add_argument('--input-points', action='store', required=True, help='input json file which includes all pre-generated points')
    parser.add_argument('-e', '--experiment', default = 'keras-example', type = str, help = 'Name of the experiment')
    parser.add_argument('-t', '--max-points', type=int, action='store', required=True, help='Maximum number of points to generate')
    parser.add_argument('-p', '--num-points', type=int, action='store', required=True, help='Number of points to generate in this iteration')
    parser.add_argument('-s', '--search-space', default = 'default', type = str, help = 'Name of search space defined in shared folder /myshare/search_space.py')
    parser.add_argument('--fix-seed', default = None, type = int, help = 'Fix random seed for reproduction')
    parser.add_argument('-o', '--output', required=True, help='output json file where outputs will be wrote; for iDDS, store it to mounted folder defined in request')

    subparsers = parser.add_subparsers(dest='library', help='HPO library')
    parser_skopt = subparsers.add_parser('skopt', help='https://scikit-optimize.github.io/stable/')
    parser_nevergrad = subparsers.add_parser('nevergrad', help='https://facebookresearch.github.io/nevergrad/')

    parser_nevergrad.add_argument('-m', '--method', default = 'RandomSearch', type = str, choices = \
                            ['ASCMA2PDEthird', 'ASCMADEQRthird', 'ASCMADEthird', 'AlmostRotationInvariantDE', 
                            'BO', 'CM', 'CMA', 'CMandAS', 'CMandAS2', 'CMandAS3', 'CauchyLHSSearch', 'CauchyOnePlusOne', 
                            'CauchyScrHammersleySearch', 'Cobyla', 'DE', 'DiagonalCMA', 'DiscreteOnePlusOne', 
                            'DoubleFastGADiscreteOnePlusOne', 'EDA', 'ES', 'FCMA', 'HaltonSearch', 'HaltonSearchPlusMiddlePoint', 
                            'HammersleySearch', 'HammersleySearchPlusMiddlePoint', 'LHSSearch', 'LargeHaltonSearch', 
                            'LhsDE', 'MEDA', 'MPCEDA', 'MetaRecentering', 'MixES', 'MultiCMA', 'MultiScaleCMA', 'MutDE', 
                            'NGO', 'NaiveIsoEMNA', 'NaiveTBPSA', 'NelderMead', 'NoisyBandit', 'NoisyDE', 'NoisyDiscreteOnePlusOne', 
                            'NoisyOnePlusOne', 'ORandomSearch', 'OScrHammersleySearch', 'OnePlusOne', 'OptimisticDiscreteOnePlusOne', 
                            'OptimisticNoisyOnePlusOne', 'PBIL', 'PCEDA', 'PSO', 'ParaPortfolio', 'Portfolio', 'Powell', 
                            'QORandomSearch', 'QOScrHammersleySearch', 'QrDE', 'RCobyla', 'RPowell', 'RSQP', 'RandomSearch', 
                            'RandomSearchPlusMiddlePoint', 'RealSpacePSO', 'RecES', 'RecMixES', 'RecMutDE', 
                            'RecombiningPortfolioOptimisticNoisyDiscreteOnePlusOne', 'RotationInvariantDE', 'SPSA', 'SQP', 
                            'SQPCMA', 'ScrHaltonSearch', 'ScrHaltonSearchPlusMiddlePoint', 'ScrHammersleySearch', 
                            'ScrHammersleySearchPlusMiddlePoint', 'Shiva', 'SplitOptimizer', 'TBPSA', 'TripleCMA', 'TwoPointsDE', 
                            'cGA', 'chainCMAPowell'], help = 'Name of the optimisation alg in nevergrad')
    main(parser.parse_args())
