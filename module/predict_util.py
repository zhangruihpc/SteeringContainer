#!/usr/bin/env python
# Rui Zhang 4.2020
# rui.zhang@cern.ch

import pickle
import json
from os import path

def DefineOptimizer(args, search_space):
    library = args.library
    max_points = args.max_points

    optimizer = None
    if library == 'skopt':
        from skopt import Optimizer
        optimizer = Optimizer(search_space, n_initial_points = max_points)
    elif library == 'nevergrad':
        import nevergrad as ng
        optimizer = ng.optimizers.registry[args.method](parametrization=search_space, budget=max_points, num_workers=1)
    else:
        assert(False), f'{library} is not supported'
    return optimizer

def UpdateOpt(old_point_name, opt_name, library):
    with open(old_point_name, 'rb') as fp:
        stored_points = pickle.load(fp)
        print('Read obj points from', old_point_name)

    assert(path.exists(opt_name)), opt_name
    with open(opt_name, 'rb') as fp:
        if library == 'skopt':
            from skopt import load
            optimizer = load(opt_name)
        elif library == 'nevergrad':
            optimizer = pickle.load(fp)

    for (stored_point, objective_point) in zip(stored_points[0], stored_points[1]):
        if library == 'skopt' and stored_point in optimizer.Xi:
            print('WARN', stored_point, 'has been evaluated before.')
            continue
        optimizer.tell(stored_point, objective_point)
    return optimizer

def SavePredictedPoints(point_name, sampled_points):
    if point_name.endswith('.pkl'):
        with open(point_name, 'wb') as fp:
            pickle.dump(sampled_points, fp)
            print('Save opt points to ', point_name)
    elif point_name.endswith('.json'):
        with open(point_name, 'w') as point_name:
            json.dump(sampled_points, point_name)
            print('Save opt points to ', point_name)

def SaveOpt(opt_name, optimizer, library):
    if library == 'skopt':
        from skopt import dump
        with open(opt_name, 'wb') as fp:
            dump(optimizer, fp)
            print('Save optimizer to ', opt_name)
    elif library == 'nevergrad':
        optimizer.dump(opt_name)
        print('Save optimizer to ', opt_name)

import numpy as np
def numpytypeconverter(obj):
    if isinstance(obj, np.integer):
        return int(obj)
    elif isinstance(obj, np.floating):
        return float(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()