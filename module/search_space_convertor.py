from skopt.space import Real, Integer, Categorical
import nevergrad as ng
import json

def retrieve_search_space(lib_name, json_name, exp_name, space_name=None):
    # Retrieve search space in json or hyperparameter_space module

    try:
        search_space, search_name = convert(lib_name, json_name, exp_name)
        if space_name:
            print(f'search space is converted from {json_name} / {exp_name}, {space_name} is not needed')
    except:
        import sys
        from os import environ
        sys.path.insert(0, environ.get('SHAREDIR'))
        from search_space import hyperparameter_space
        search_space = hyperparameter_space[lib_name][space_name]
        search_name = None

    return search_space, search_name


def convert(lib_name, json_name, exp_name, prettyprint=False):
    assert (lib_name in ['skopt', 'nevergrad']), f'{lib_name} is not supported'

    with open(json_name, 'r') as fp:
        search_space_json = json.load(fp)
    assert (exp_name in search_space_json.keys()), f'{exp_name} is not defined in {json_name}, {search_space_json.keys()}'
    search_space_json = search_space_json[exp_name]
    if prettyprint: print(exp_name.join(['"', '":']), json.dumps(search_space_json, indent=2), '\n')

    methods_map = {}

    if lib_name == 'skopt':
        def categorical(label, categories):
            return label, Categorical(name=label, categories=categories)

        def uniformint(label, low, high):
            return label, Integer(name=label, low=low, high=high, prior="uniform")

        def uniform(label, low, high):
            return label, Real(name=label, low=low, high=high, prior='uniform')

        def loguniformint(label, low, high, base=10):
            return label, Integer(name=label, low=low, high=high, prior="log-uniform", base=base)

        def loguniform(label, low, high, base=10):
            return label, Real(name=label, low=low, high=high, prior='log-uniform', base=base)

        methods_map = {
            'categorical': categorical,
            'uniformint': uniformint,
            'uniform': uniform,
            'loguniformint': loguniformint,
            'loguniform': loguniform,
        }

    elif lib_name == 'nevergrad':
        def categorical(label, categories):
            print('function categorical:', label, categories)
            return label, ng.p.TransitionChoice(choices=categories),

        def uniform(label, low, high):
            return label, ng.p.Scalar(lower=low, upper=high)

        def loguniform(label, low, high, base=10):
            return label, ng.p.Log(lower=low, upper=high, exponent=base),

        methods_map = {
            'categorical': categorical,
            'uniformint': uniform,
            'uniform': uniform,
            'loguniformint': loguniform,
            'loguniform': loguniform,
        }


    search_space_dict = {}
    for hp in search_space_json:
        assert (hp in search_space_json), f'{hp} does not supported, please check your {json_name}'
        assert ('type' in search_space_json[hp]), f'{hp} does not have type, please check your {json_name}'
        assert ('range' in search_space_json[hp]), f'{hp} does not have range, please check your {json_name}'
        method = search_space_json[hp]['type']
        assert (method.lower() in methods_map), f'{hp} ({method}) type is not supported. supported types are {methods_map}. please check your {json_name}'
        space_name, space_value = methods_map[method.lower()](label = hp, **search_space_json[hp]['range'])
        search_space_dict[space_name] = space_value

    search_space_converted = list(search_space_dict.values()) if lib_name == 'skopt' else ng.p.Instrumentation(**search_space_dict)
    search_space_name = list(search_space_dict.keys()) if lib_name == 'skopt' else None

    return search_space_converted, search_space_name

def validate_json(args):
    lib_name = args.library
    search_space_converted, search_space_name = convert(lib_name, args.input, args.experiment, True)
    if lib_name == 'skopt':
        for i, j in zip(search_space_name, search_space_converted):
            print(i, j)
    elif lib_name == 'nevergrad':
        print(search_space_converted[1])
    print(f'{args.input}/{args.experiment} is legal under the {args.library} syntax.')

if __name__ == '__main__':
    from argparse import ArgumentParser
    
    """Validate json file."""
    parser = ArgumentParser(description="Test legality of search space defined in json file")
    parser.add_argument('-l', '--library', default = 'skopt', type = str, choices = ['skopt', 'nevergrad'], help = 'Name of the library to test against')
    parser.add_argument('-i', '--input', default = '../share/search_space.json', type = str, help = 'Name of the json file')
    parser.add_argument('-e', '--experiment', default = 'keras-example', type = str, help = 'Name of the experiment')

    validate_json(parser.parse_args())
